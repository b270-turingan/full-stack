// console.log("Hello World");

// For selecting HTML elements, we'll be using: document.querySelector
/*
	Syntax:
		document.querySelector("htmlElement");

	"document" - refers to the whole page
	".querySelector" - used to select a specific object/HTML element from the document.
*/

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// [SECTION] Event Listeners
	// Whenever a user interacts with a web page, this action is considered as an event.
	// The method used is "addEventListener"
	// For selecting HTML elements, we'll be using: document.querySelector
	// Syntax: document.querySelector("htmlElement");
		/*
			"document" - refers to the whole page
			".querySelector" - used to select a specific object/HTML element from the document
		*/
// EXAMPLE:

/*txtFirstName.addEventListener('keyup', (event) => {

	// The "innerHTML" property sets or returns the value of the element
	    // The ".value" property sets or returns the value of an attribute. (form control elements: input, select, etc.)

	spanFullName.innerHTML = txtFirstName.value;
})*/

// Multiple listeners can also be assigned to the same event
txtFirstName.addEventListener("keyup", (ever) => {

	// event.target contains the element where the event happened.
	console.log(event.target);
	// event.target.value gets the value of the input
	console.log(event.target.value);
});

//[ACTIVITY]

/*
	Instead of anonymous functions for each of the event listener:
     - Create a function that will update the span's contents based on the value of the first and last name input fields.
     - Instruct the event listeners to use the created function.
*/


function fullName() {

	const firsName = txtFirstName.value;
	const lastName = txtLastName.value
	spanFullName.innerHTML = `${firsName} ${lastName}`;
}

txtFirstName.addEventListener('keyup', fullName);
txtLastName.addEventListener('keyup', fullName);