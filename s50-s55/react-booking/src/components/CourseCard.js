import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {

    console.log(courseProp);

    // Deconstructs the course properties into their own variables
    const{ _id, name, description, price} = courseProp;

    
    return (
        <Card>
        <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PhP {price}</Card.Text>
        <Card.Subtitle>Enrollees</Card.Subtitle>
        <Button variant="primary" className="mt-2" as={Link} to={`/courses/${_id}`}>Details</Button>
        </Card.Body>
        </Card>
    )
}
