import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){
	// Redirect back to login
	const { unsetUser, setUser } = useContext(UserContext);

	// Clear the localStorage
	unsetUser();

	useEffect(() => {
		setUser({id: null})
	})
	return(

		<Navigate to ='/login' />
	)
}